from airflow import DAG

from airflow.contrib.operators.gcs_to_bq import GoogleCloudStorageToBigQueryOperator
from airflow.contrib.operators.bigquery_operator import BigQueryOperator

from airflow.utils.dates import days_ago

default_arguments = {'owner': 'Richard Mendoza', 'start_date': days_ago(1)}

with DAG(
    'bigquery_data_load', 
    schedule_interval='@hourly',
    catchup=False,
    default_args=default_arguments,
) as dag:
    
    load_data = GoogleCloudStorageToBigQueryOperator(
        task_id = 'load_data',
        bucket = 'python-files-datahack',
        source_objects=['data/cardata/*'],
        source_format='CSV',
        skip_leading_rows=1,
        destination_project_dataset_table='healthy-display-272415.fact_project.m_history_vehicle_richard',
        create_disposition='CREATE_IF_NEEDED',#Airflow creates the table,
        write_disposition='WRITE_APPEND',
        bigquery_conn_id='google_cloud_default_richard',
        google_cloud_storage_conn_id='google_cloud_default_richard'
    )

    query = '''
    SELECT * EXCEPt (rank)
    FROM (
        SELECT 
            *, 
            ROW_NUMBER() OVER(
            PARTITION BY vehicle_id ORDER BY DATETIME(date, TIME(hour, minute, 0)) DESC
        ) AS rank
        FROM `healthy-display-272415.fact_project.m_history_vehicle_richard`
    ) as latest
    WHERE rank = 1;

    '''
#export AIRFLOW_HOME=/home/hola/airflow-tutorial
    create_table = BigQueryOperator(
        task_id='create_table',
        sql=query,
        destination_dataset_table='healthy-display-272415.fact_project.m_history_vehicle_out_richard2',
        write_disposition='WRITE_TRUNCATE',
        create_disposition='CREATE_IF_NEEDED',
        use_legacy_sql=False,
        location='US',
        bigquery_conn_id='google_cloud_default_richard'
    )

load_data >> create_table
